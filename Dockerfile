FROM python:2.7

WORKDIR /geoevent

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

# https://forums.docker.com/t/import-data-on-mongodb-from-docker-compose/5200
