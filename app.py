import os
from bson.json_util import dumps
from flask import Flask, jsonify, redirect, url_for, request, render_template
from flask_googlemaps import GoogleMaps
# pip install geopy
from flask_bootstrap import Bootstrap
from pymongo import MongoClient
from datetime import datetime
import logging

logging.basicConfig(level=logging.DEBUG,
					format='%(asctime)s %(levelname)s %(message)s',
					filename='debug.log',
					filemode='w')

app = Flask(__name__)
Bootstrap(app)
GoogleMaps(app)

client = MongoClient(
	os.environ['DB_PORT_27017_TCP_ADDR'],
	27017)
db = client.geoevent_db


@app.route('/newFirst', methods=['POST'])
def newFirst():

	# with open('db/initial_db.json') as f:
	#     file_data = json.load(f)
	event_1 = {"name": "Initial evnet",
			  "type": "walk",
			  "start_date": "2018/09/10 12:40",
			  "end_date": "2018/09/10 12:45",
			  "geolocation": [
			         {
			             "source_date" : ("2018-09-10T12:40:00.000Z"),
			             "source": "browser",
			             "type": "Point",
			             "coordinates": [-33.9, 18.4]
			         },
			         {
			 			"source_date" : ("2018-09-10T12:42:00.000Z"),
			             "source": "browser",
			             "type": "Point",
			             "coordinates": [-33.92, 18.46]
			         },
			         {
			 			"source_date" : ("2018-09-10T12:45:00.000Z"),
			             "source": "browser",
			             "type": "Point",
			             "coordinates": [-33.9, 18.4]
			         }
			     ]
			}
	events_coll = db.event_collection
	events_coll.insert(event_1)

	return redirect(url_for('events'))

@app.route('/')
def events():

	events_doc = db.geoevent_db.find()
	events = [event for event in events_doc]
	logging.info(("Saved events: " + dumps(events, indent=2)))


	return render_template('index.html', events=events)


# Get events
@app.route('/api/v1.0/event', methods=['GET'])
def get_all_events():
	'''Get all the events'''
	events = db.geoevent_db.find()
	logging.info(('events: ', events))
	output = []
	for event in events:
		output.append({'name' : event['name'],
						'description' : event['description']})

	return jsonify({'result' : output})

@app.route('/api/v1.0/event/<string:name>', methods=['GET'])
def get_single_event(name):
	'''Get a single event'''
	event = db.geoevent_db.find_one({'name' : name})
	if event:
		output = {'name' : event['name'],
				'description' : event['description']}
	else:
		output = 'Event not found'

	return jsonify({'result' : output})

# Post event
@app.route('/api/v1.0/event', methods=['POST'])
def create_event():
	'''Insert event into db'''
	# TODO: add validation on client side / js
	logging.info('call successful')
	name = request.form['name']
	type = request.form['type']
	lat = request.form['startLat']
	long = request.form['startLong']

	new_event = {
			'name': name,
			'type': type,
			'start_date': datetime.now(),
			'end_date': datetime.now(),
			'geolocation': [
				   {
					   'source_date' : datetime.now(),
					   'source': 'browser',
					   'type': 'Point',
					   'coordinates': [long, lat]
				   }]

		}
	logging.info(("new event: " + dumps(new_event, indent=2)))

	db.geoevent_db.insert_one(new_event)
	#
	# Return new event should call a get_single_event repeated code
	event = db.geoevent_db.find_one({'name' : name})
	if event:
		output = {'name' : event['name'],
				'type' : event['type']}
	else:
		output = 'Event not inserted'
	#
	return jsonify({'result' : output})


if __name__ == "__main__":
	app.run(host='0.0.0.0', debug=True)
