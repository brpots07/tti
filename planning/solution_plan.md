# Requirements
## RESTful service

Python RESTful API:
 - store (put)
 - retrieving (get)

event 1-n geolocation
### Event
 {
  name : "event_name",
  type : "event_type"
  start_date : "event_start_date", -- MIN(geo_source_date)
  enddate: "event_end_date"	-- MAX(geo_source_date)
}

### geolocation
{
 longitude : "geo_longitude",
 latitude : "geo_latitude",
 source: "browser"	-- default
 source_date: "geo_source_date"
 event_id: "geo_event_id"
}


Python Package Preferences (optional):
Tornado vs Flask
Click
Cerberus

## Storage:
Design SQL db and convert to NoSQL db.
### SQL db
Basic design done using ERR diagram. See diagram /planning/SQL_model.pdf
### NoSQL
Use Mongodb -> no particular reason

NoSQL structure:
Best choice for one-to-many relationship. See https://docs.mongodb.com/v3.2/applications/data-models-relationships/
No time to investigate the best choice here, but decision should be based optimal schema storing, retrieving the data and how to update event dates based on new records/documents inserted in geolocations .


## Frontend:
Build an HTML / CSS / Javascript (bonus: angular 5) frontend for displaying a map of the geolocations calling the API

## Orchestration / Documentation:
Please document your API in the form of a README file as well as provide instructions on how to set up the project. Bonus: dockerize it in order to have it run in a single command.

## Deadline:
README:
- what would have liked to do
Investigate best NoSQL schema
- didn't know before exercise
NoSQL. Angular and still don't
Its been a long time since created RESTful API
- Anything do differently


## Compentencies being measured:
Building a RESTful service
NoSQL competency
Documentation
Following a spec
