# Requirements
## RESTful service
Build a Python RESTful API for storing and retrieving events and geolocations. Each event should have a “name” and a date when the event happened (can be a walk, drive or whatever kind of event), a “date” when the event started and a “date” when it ended. Each event should have geolocations related to it. A one to many relationship. Geolocations should have “longitude” and “latitude” properties, as well as a “source” describing what type of device sent the geolocation (for the purposes of this exercise possible values are only “browser”), the date that the geolocation was set as well as a required property to relate it to an event (if there are other ways to relate it to an event). The geolocation date should effect and adjust the the start and end date’s of the event.
Python Package Preferences (optional):
Tornado
Click
Cerberus

## Storage:
Favourably use a NoSQL backend technology (mongo, couchbase, elasticsearch etc), otherwise store in memory how you would structure the NoSQL data.
## Frontend:
Build an HTML / CSS / Javascript (bonus: angular 5) frontend for displaying a map of the geolocations calling the API
## Orchestration / Documentation:
Please document your API in the form of a README file as well as provide instructions on how to set up the project. Bonus: dockerize it in order to have it run in a single command.
## Deadline:
If you can have it ready by Friday, that would be great. If you cannot have it ready by then, submit what code you have done already and include in the README what you still would do in a final version, any learnings from it, anything you didn’t know before you started the exercise and anything you would have done differently had you more time or from what you learnt.
## Compentencies being measured:
Building a RESTful service
NoSQL competency
Documentation
Following a spec
