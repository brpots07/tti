# geolocation event

This is a RESTful API used to track an event's geolocations. These events can be walking, run, driving etc.
This is a dockerized app. So to run please ensure you have docker install (add link).

Google Api (https://github.com/googlemaps/google-maps-services-python)

Go to projects root directory and run
docker-compose build
docker-compose up

To auto-seed db initially https://forums.docker.com/t/import-data-on-mongodb-from-docker-compose/5200
