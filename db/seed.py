import json
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.geoevent_db

collection_event = db['event']

with open('initial_db.json') as f:
    file_data = json.load(f)


collection_event.insert(file_data)
client.close()
