from pymongo import MongoClient
import datetime

client = MongoClient(
	os.environ['DB_PORT_27017_TCP_ADDR'],
	27017)
db = client.geoevent_db

class geolocation(db.EmbeddedDocument):
	start_date = db.DateTimeField(required=True, default=datetime.datetime.utcnow),
	source = db.StringField(required=True, default='browser')


class Event(db.Document):
    name = db.StringField(required=True)
    type = db.StringField(max_length=50)
    start_date = db.DateTimeField(required=True, default=datetime.datetime.utcnow)
    end_date = db.DateTimeField(required=True, default=datetime.datetime.utcnow)
	geolocation = db.EmbeddedDocumentField(geolocation)
